// Importar el modelo de la base de datos para el CRUD
const modeloProducto = require('../models/modelProducts');


// Exportar en diferentes variables los métodos para el CRUD

// CRUD => Create
exports.crear = async (req, res) => {
    
    try {
        
        let producto;

        console.log("req.body:", req.body);
        producto = new modeloProducto(req.body);
        // producto = new modeloProducto({
        //     id: 6,
        //     nombre: "Chatas",
        //     cantidad: 36,
        //     categoria: "Carnes",
        //     precio: 23000
        // });

        await producto.save();

        res.send(producto);

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al guardar producto.');
    }
}


// CRUD => Read
exports.obtener = async (req, res) => {

    try {
        
        const producto = await modeloProducto.find();
        res.json(producto);

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al obtener el/los producto(s).');
    }
}


// CRUD => Update
exports.actualizar = async (req, res) => {

    try {
        
        const producto = await modeloProducto.findById(req.params.id);

        // console.log(producto);

        if (!producto){
            console.log(producto);
            res.status(404).json({msg: 'El producto no existe.'});
        }
        else{
            // await modeloProducto.findByIdAndUpdate({_id: req.params.id}, {cantidad: 66});
            await modeloProducto.findByIdAndUpdate({_id: req.params.id}, req.body);
            res.json({msg: 'Producto actualizado correctamente'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al editar el producto.');
    }
}


// CRUD => Delete
exports.eliminar = async (req, res) => {

    try {
        const producto = await modeloProducto.findById(req.params.id);

        // console.log(producto);

        if (!producto){
            console.log(producto);
            res.status(404).json({msg: 'El producto no existe.'});
        }
        else{
            await modeloProducto.findByIdAndRemove({_id: req.params.id});
            res.json({mensaje: 'Producto eliminado correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar el producto.')
    }
}