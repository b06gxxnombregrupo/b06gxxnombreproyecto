// Importar express - sintaxis ES6 => estándar de JavaScript
// import express from 'express'
// Sintaxis nativa de nodejs
const express = require('express');

const router = express.Router();

// // Importar módulo de MongoDB => mongoose
// const mongoose = require('mongoose');

// Importar dependencias variable de entorno
// require('dotenv').config({path: 'var.env'})
const conectarDB = require('./config/db')

let app = express();

// app.use('/', function(req, res){
//     res.send('Hola Tripulantes')
// });


// uso de archivos tipo json en la app
app.use(express.json());

app.use(router);

// Conexión Base datos
conectarDB();
// mongoose.connect(process.env.URL_MONGODB)
//         .then(function(){console.log("Conexión Establecida con MongoDB Atlas")})
//         .catch(function(e){console.log(e)});


// Integración del Frontend en el Backend
app.use(express.static('public'));


// // modelo squema (schema) de la base de datos
// const productSchema = new mongoose.Schema({
//     id: Number,
//     nombre: String,
//     cantidad: Number,
//     categoria: String,
//     precio: Number
// });

// // modelo del producto => tener en cuenta el esquema para la colección...
// // const modeloProducto = mongoose.model('modelo_prods', productSchema);
// const modeloProducto = mongoose.model('productos', productSchema);


// // CRUD => Create
// modeloProducto.create(
//     {
//         id: 6,
//         nombre: "Jabón",
//         cantidad: 45,
//         categoria: "Aseo",
//         precio: 3000
//     },
//     (error) => {
//         // console.log("Ocurrió el siguiente error");
//         if (error) return console.log(error);
//         // console.log(error);
//         // console.log("Fin del Error");
//     }
// );

// -------------------------------------------
// ######  Descentralizar el CRUD  ###########
// -------------------------------------------
// // CRUD => Read
// modeloProducto.find((error, productos) => {
//     if (error) return console.log(error);
//     console.log(productos);
// });


// // CRUD => Update
// modeloProducto.updateOne({id: 1}, {cantidad: 4}, (error) => {
//     if (error) return console.log(error);
// });


// // CRUD => Delete
// modeloProducto.deleteOne({id: 1}, (error) => {
//     if (error) return console.log(error);
// });




// -------------------------------------------
// ######  Rutas Respecto al CRUD  ###########
// -------------------------------------------

// // uso de archivos tipo json en la app
// app.use(express.json());
// CORS (Cross-Origin Resource Sharing) => mecanismo o reglas de seguridad para el control de peticiones http
const cors = require('cors');
app.use(cors());

// Solución temporal al erros CORS
var whitelist = ['http://localhost:4000/', 'http://localhost:4200/']
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { Origin: true } // reflect (enable) the requested Origin in the CORS response
  }else{
    corsOptions = { Origin: false } // disable CORS for this request
  }
  callback(null, corsOptions) // callback expects two parameters: error and options
}

// solicitudes al CRUD => el controlador 
const crudProductos = require('./controllers/controlProducts');


// Establecer las rutas respecto al CRUD
// CRUD => Create
router.post('/apirest/', cors(corsOptionsDelegate), crudProductos.crear);
// CRUD => Read
router.get('/apirest/', cors(corsOptionsDelegate), crudProductos.obtener);
// CRUD => Update
router.put('/apirest/:id', cors(corsOptionsDelegate), crudProductos.actualizar);
// CRUD => Delete
router.delete('/apirest/:id', cors(corsOptionsDelegate), crudProductos.eliminar);

// // Establecer las rutas respecto al CRUD
// // CRUD => Create
// router.post('/', crudProductos.crear);
// // CRUD => Read
// router.get('/', crudProductos.obtener);
// // CRUD => Update
// router.put('/:id', crudProductos.actualizar);
// // CRUD => Delete
// router.delete('/:id', crudProductos.eliminar);







// router.get('/met', function(req, res){
//     res.send("Estoy utilizando el método GET");
// });

// // Método GET con función flecha
// router.get('/met', (req, res) => {
//     res.send("Estoy utilizando el método GET");
// });

// router.post('/metodopost', function(req, res){
//     res.send("Utilizando el método POST")
//     // Conexión con la base de datos
//     // console.log("Antes de la Conexión a la DB")
//     // const user = 'b06gxx';
//     // const psw = 'b06gxx123';
//     // const db = 'b06gxx';
//     // const url = `mongodb+srv://${user}:${psw}@misiontic-uis-jas.krymo.mongodb.net/${db}?retryWrites=true&w=majority`;

//     // mongoose.connect(url)
//     //     .then(function(){console.log("Conexión Establecida con MongoDB Atlas")})
//     //     .catch(function(e){console.log(e)});
    
//     // res.send("Utilizando el método POST")
//     // console.log("Después de la Conexión a la DB")
// });


// Conexión con la base de datos
// const user = 'b06gxx';
// const psw = 'b06gxx123';
// const db = 'b06gxx';
// const url = `mongodb+srv://${user}:${psw}@misiontic-uis-jas.krymo.mongodb.net/${db}?retryWrites=true&w=majority`;

// mongoose.connect(url)
//     .then(function(){console.log("Conexión Establecida con MongoDB Atlas")})
//     .catch(function(e){console.log(e)})



// app.listen(4000);
app.listen(process.env.PORT);

console.log("La aplicación se ejecuta en: http://localhost:4000");

