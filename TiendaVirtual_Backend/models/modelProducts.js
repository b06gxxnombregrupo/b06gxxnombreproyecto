// Importar módulo de MongoDB => mongoose
const mongoose = require('mongoose');


// Schema => estructura tipo JSON que define la estructura de la base de datos.
const productSchema = mongoose.Schema({

    id: String,
    nombre: String,
    cantidad: String,
    categoria: String,
    precio: String,
    urlimg: String
    
},
{
    versionKey: false,
    timestamps: true
});

// modelo del producto => tener en cuenta el esquema para la colección...
module.exports = mongoose.model('productos', productSchema);