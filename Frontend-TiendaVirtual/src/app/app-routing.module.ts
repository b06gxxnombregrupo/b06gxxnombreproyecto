import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { CommonModule } from '@angular/common';


const routes: Routes = [
  // Agregar las rutas hijas
  // {
  //   path: 'home',
  //   loadChildren: () => import('./@public/pages/home/home.module').then(m => m.HomeModule)
  // },
  // {
  //   path: 'contact',
  //   loadChildren: () => import('./@public/pages/contact/contact.module').then(m => m.ContactModule)
  // },
  // {
  //   path: 'dashboard',
  //   loadChildren: () => import('./@admin/pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  // },
  // {
  //   path: 'users',
  //   loadChildren: () => import('./@admin/pages/users/users.module').then(m => m.UsersModule)
  // },
  {
    // rutas vacías
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    // ruta comodín
    path: '**',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [],
  imports: [
    // CommonModule
    RouterModule.forRoot(routes,
      {
        useHash: true, // evitar que recargue las páginas
        // Para cuando hay bastante contenido en una página, restaure la posición...
        scrollPositionRestoration: 'enabled'
      })
  ],
  exports: [RouterModule]  
})
export class AppRoutingModule { }
