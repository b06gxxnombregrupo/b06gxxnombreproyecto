import { Component, OnInit } from '@angular/core';
import { audit } from 'rxjs';
import { ProductsService } from 'src/app/@services/products.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  // Atributos
  formEditar: boolean = false;

  _id: any;
  id: any;
  nombre: any;
  cantidad: any;
  categoria: any;
  precio: any;
  urlimg: any;

  productos: any;
  // productos: any [] = [
  //   {
  //     id: 1,
  //     nombre: "Lomo",
  //     categoria: "Carnes"
  //   },
  //   {
  //     id: 2,
  //     nombre: "Jabón",
  //     categoria: "Aseo"
  //   }
    
  // ]


  constructor(private _producto: ProductsService) { }

  ngOnInit(): void {
    this.obtenerProd();
  }

  // Métodos o Funciones

  // Activar Formulario Editar Producto
  editarForm(_id: any, id: any, nombre: any, cantidad: any, categoria: any, precio: any, urlimg: any){
    this.formEditar = true;
    this._id = _id;
    this.id = id;
    this.nombre = nombre;
    this.cantidad = cantidad;
    this.categoria = categoria;
    this.precio = precio;
    this.urlimg = urlimg;
  }

  cancelarEditForm(){
    this.formEditar = false;
    this._id = "";
    this.id = "";
    this.nombre = "";
    this.cantidad = "";
    this.categoria = "";
    this.precio = "";
    this.urlimg = "";
  }

  // Garudar productos
  guardarProducto(){
    for(let i = 0; i< 3; i++){
      console.log("i = ", i+1);
    }
    console.log("Variable Nombre:", this.nombre);
    console.log("Variable Categoría:",this.categoria);
    
    if(this.nombre != undefined){
      let ind = this.productos.length + 1;

      let prodAux = {
        id: ind,
        nombre: this.nombre,
        cantidad: this.cantidad,
        categoria: this.categoria,
        precio: this.precio,
        urlimg: this.urlimg
      }

      // this.productos.push(prodAux); //arreglo de productos
      this._producto.guardarDatos(prodAux)
        .subscribe(datos => {
          console.log("Datos guardados:", datos);
          this.obtenerProd();
        })
      
    }
  }


  // Función Actualizar 
  actualizarProducto(){
    let prodAux = {
      id: this.id,
      nombre: this.nombre,
      cantidad: this.cantidad,
      categoria: this.categoria,
      precio: this.precio,
      urlimg: this.urlimg
    }

    this._producto.actualizarDatos(this._id, prodAux)
      .subscribe(datos => {
        console.log("Producto actualizardo...", datos);
        this.obtenerProd();
      })
    
    this.formEditar = false;
    this._id = "";
    this.id = "";
    this.nombre = "";
    this.cantidad = "";
    this.categoria = "";
    this.precio = "";
    this.urlimg = "";
  }


  // Función Eliminar
  eliminar(indice: any){
    
    console.log("Elemento a eliminar: ", indice)
    // elimina elemento del arreglo
    // this.productos.splice(indice, 1);
    this._producto.eliminarDatos(indice)
      .subscribe(datos => {
        console.log("Dato eliminado:", indice);
        this.obtenerProd();
      })
    
  }

  obtenerProd(){
    this._producto.obtenerDatos()
      .subscribe(datos => {
        this.productos = datos;
        console.log(this.productos);
      })
  }

}
